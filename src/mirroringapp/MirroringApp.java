/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mirroringapp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import remote_interfaces.RemoteFileInterface;
import remote_interfaces.RemoteListener;

/**
 *
 * @author darglk
 */
public class MirroringApp extends UnicastRemoteObject implements RemoteFileInterface  {
    /**
    * Zmienna przechowująca wszystkich podłączonych do serwera klientów.
    */
    private final ArrayList<RemoteListener> listOfClients = new ArrayList<>();
    /**
    * Metoda wysłająca wiadomość tekstową do klientów.                          
    * <p>
    * Metoda ta ma za zadanie wysłać wiadomość tekstową do wszystkich klientów w kolekcji
    * listOfClients. Rozsyłanie odbywa się w osobnym wątku.
    * @param  msg zawiera zawartość wiadomości.
    */
    private void notifyListeners(String msg){
        for(RemoteListener remoteListener : this.listOfClients){
            Thread t = new Thread(new Runnable(){
                public void run(){
            
                try{
                    remoteListener.sendMessageToClients(msg);
                }
                catch(RemoteException e){
                    System.out.println("removing listener -"
                            + remoteListener);
                    listOfClients.remove(remoteListener);
                    }
                }
            });
            t.start();
        }
    }
    /**
    * Metoda wysłająca wczytany plik pozostałych klientów.                          
    * <p>
    * Metoda ta ma za zadanie wysłać plik wczytany od klienta do wszystkich klientów w kolekcji
    * listOfClients. Rozsyłanie odbywa się w osobnym wątku.
    * @param  fileName zawiera nazwę załadowanego pliku.
    */
    private void uploadToAllListeners(String fileName){
        for(RemoteListener remoteListener : this.listOfClients){
                Thread t = new Thread(new Runnable(){
                    public void run(){
                        try {
                            remoteListener.downloadFile(fileName);
                        } catch (RemoteException ex) {
                            listOfClients.remove(remoteListener);
                        }
                    }
                });
                t.start();
        }
    }
    /**
    *Konstruktor klasy MirroringApp.
    */
    public MirroringApp() throws RemoteException{
        System.out.println("Loading mirroring app...");
    }
    /**
    *Metoda main uruchamiająca program i nasłuchiwanie na przybycie klientów.
    */
    public static void main(String[] args)  {
        try {
            MirroringApp server = new MirroringApp();
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("remoteFileInterface", server);
        } catch (RemoteException re) {
            System.err.println("Remote Error - " + re);
        } catch (Exception e) {
            System.err.println("Error - " + e);
        }
    }
    /**
    * Przeciążona metoda z interfejsu RemoteFileInterface.   .                       
    * <p>
    * Metoda ta ma za zadanie dodać klienta do listy.
    * </p>
    * @param  listener zawiera referencję do klienta łączącego się z serwerem.         
    */
    @Override
    public void addRemoteListener(RemoteListener listener) throws RemoteException {
       this.listOfClients.add(listener);
    }
    /**
    * Przeciążona metoda z interfejsu RemoteFileInterface.   .                       
    * <p>
    * Metoda ta ma za zadanie usunąć klienta z listy.
    * </p>
    * @param  listener zawiera referencję do klienta.         
    */
    @Override
    public void removeRemoteListener(RemoteListener listener) throws RemoteException {
        this.listOfClients.remove(listener);
    }
    
    /**
    * Przeciążona metoda z interfejsu RemoteFileInterface.   .                       
    * <p>
    * Metoda ta ma za zadanie wysłać wiadomość tekstową do wszystkich klientów.
    * </p>
    * @param  msg zawiera zawartosć wiadomości.         
    */
    @Override
    public void sendMessageToClients(String msg) throws RemoteException {
        this.notifyListeners(msg);
    }
    
    /**
    * Przeciążona metoda z interfejsu RemoteFileInterface.   .                       
    * <p>
    * Metoda ta ma za zadanie pobrać plik ze zdalnego serwera.
    * </p>
    * @param  fileName zawiera nazwę pliku do załadowania.         
    * @return Zwraca tablicę bajtów z których plik się składa.
    */
    @Override
    public byte[] downloadFile(String fileName){
      try {
         File file = new File(fileName);
         byte buffer[] = new byte[(int)file.length()];
         BufferedInputStream input = new
         BufferedInputStream(new FileInputStream(fileName));
         input.read(buffer,0,buffer.length);
         input.close();
         return(buffer);
      } catch(Exception e){
         JOptionPane.showMessageDialog(null, "There was an error while downloading file.","Download Error",
         JOptionPane.ERROR_MESSAGE);
         return(null);
      }
   }
    
    /**
    * Przeciążona metoda z interfejsu RemoteFileInterface.   .                       
    * <p>
    * Metoda ta ma za zadanie załadować plik ze zdalnego serwera.
    * </p>
    * @param  fileName zawiera nazwę pliku do załadowania.         
    * @param buffer zawiera tablicę bajtów z których składa się plik.
    */
    @Override
    public void uploadFile(String fileName, byte[] buffer) throws RemoteException {
         try {
            File file = new File(fileName);
            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(fileName));
            output.write(buffer,0,buffer.length);
            output.flush();
            output.close();
            this.uploadToAllListeners(fileName);
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "There was an error while uploading file.","Upload Error",
            JOptionPane.ERROR_MESSAGE);
        }
    }
}

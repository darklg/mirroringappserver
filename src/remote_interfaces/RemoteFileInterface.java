/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remote_interfaces;

/**
 *
 * @author darglk
 */
public interface RemoteFileInterface extends java.rmi.Remote {
    public void addRemoteListener (RemoteListener listener ) throws java.rmi.RemoteException;
    public void removeRemoteListener (RemoteListener listener ) throws java.rmi.RemoteException;
    public void sendMessageToClients(String msg) throws java.rmi.RemoteException;
    public byte[] downloadFile(String fileName) throws java.rmi.RemoteException;
    public void uploadFile(String fileName, byte[] buffer) throws java.rmi.RemoteException;
}
